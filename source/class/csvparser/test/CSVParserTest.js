/*
 * Copyright (c) 2012 Washington University
 *
 * Author: Kevin A. Archie <karchie@wustl.edu>
 */

qx.Class.define("csvparser.test.CSVParserTest",
{
  extend : qx.dev.unit.TestCase,

  members :
  {
    testParse : function() {
      var parser = new csvparser.CSVParser();
      var parsed = parser.csvToArray('a,b,c\nd,e,f');
      this.assertEquals(2, parsed.length);
      this.assertEquals(3, parsed[0].length);
      this.assertEquals('a', parsed[0][0]);
      this.assertEquals('b', parsed[0][1]);
      this.assertEquals('c', parsed[0][2]);
      this.assertEquals(3, parsed[1].length);
      this.assertEquals('d', parsed[1][0]);
      this.assertEquals('e', parsed[1][1]);
      this.assertEquals('f', parsed[1][2]);
    },

    testParseEmpty : function() {
      var parser = new csvparser.CSVParser();
      var parsed = parser.csvToArray('');
      this.assertEquals(0, parsed.length);
    },
    
    testParseWithQuotes : function() {
      var parser = new csvparser.CSVParser();
      var parsed = parser.csvToArray('"1","2"\n"A","B"\n');

      this.assertEquals(2, parsed.length);
      this.assertEquals(2, parsed[0].length);
      this.assertEquals('1', parsed[0][0]);
      this.assertEquals('2', parsed[0][1]);
      this.assertEquals(2, parsed[1].length);
      this.assertEquals('A', parsed[1][0]);
      this.assertEquals('B', parsed[1][1]);
    },

    testParseWithQuotedNewline : function() {
      var parser = new csvparser.CSVParser();
      var parsed = parser.csvToArray('"foo bar", "baz\nyak", "pththpthppt"\n1,2,3\nA,B,C');

      this.assertEquals(3, parsed.length);
      this.assertEquals(3, parsed[0].length);
      this.assertEquals('foo bar', parsed[0][0]);
      this.assertEquals('baz\nyak', parsed[0][1]);
      this.assertEquals('pththpthppt', parsed[0][2]);
      this.assertEquals(3, parsed[1].length);
      this.assertEquals('1', parsed[1][0]);
      this.assertEquals('2', parsed[1][1]);
      this.assertEquals('3', parsed[1][2]);
      this.assertEquals(3, parsed[2].length);
      this.assertEquals('A', parsed[2][0]);
      this.assertEquals('B', parsed[2][1]);
      this.assertEquals('C', parsed[2][2]);
    }
  }
});
