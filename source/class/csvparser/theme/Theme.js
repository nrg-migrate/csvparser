/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("csvparser.theme.Theme",
{
  meta :
  {
    color : csvparser.theme.Color,
    decoration : csvparser.theme.Decoration,
    font : csvparser.theme.Font,
    icon : qx.theme.icon.Tango,
    appearance : csvparser.theme.Appearance
  }
});