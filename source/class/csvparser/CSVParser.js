/**
 * This class implements parsing CSV data. For better browsers it can run straight through the
 * data in a single blocking loop. For slow browsers there is a progressive version of the parser
 * which gives back control to the browser after each record. 
 */
qx.Class.define("csvparser.CSVParser",
{
  extend : qx.core.Object,
  construct : function () {
    this.setEolRegExp(new RegExp("(\\r?\\n)", "g"));
  },
  properties : {
    /**
     * A regular expression object that looks for the end of a line.
     */
    eolRegExp : {
      deferredInit : true
    }
  },
  statics : {
    /**
     * Converts a CSV separated line into a list of records
     *
     * @param row {Array} Row to parse
     */
    fields : function (row) {
      var position = {
	data : row,
	ret : []
      };
      var closeQuoteIndex = function (row) {
	var nextQuote = row.indexOf("\"");
	
	var done = false;
	while (!done) {
	  if (nextQuote != -1 && row[nextQuote + 1] != undefined && row[nextQuote +1] == "\"") {
	    nextQuote = row.indexOf("\"", nextQuote + 1 + 1);
	  }
	  else {
	    done = true;
	  }
	}
	return nextQuote;
      };

      var findComma = function (row) {
	return row.indexOf(",");
      };

      var trimHead = function (str) {
	return str.replace(/^\s+/g, '') ;
      };
      
      var unescapeQuote = function (str) {
	return str.replace(/\"\"/g, "\"");
      };

      var done = false;
      while(!done) {
	if (position.data.substring(0,1) == "\"") {
	  position.data = position.data.substring(1);
	  var closed = closeQuoteIndex(position.data);
	  if (closed != -1) {
	    position.ret.push(position.data.substring(0,closed));
	    position.data = trimHead(position.data.substring(closed+1));
	    var nextComma = findComma(position.data);
	    if (nextComma != -1) {
	      position.data = trimHead(position.data.substring(nextComma + 1));
	    }
	    else {
	      done = true;
	    }
	  }
	  else {
	    position.ret.push(position.data);
	    done = true;
	  }
	}
	else {
	  var commaIndex = findComma(position.data);
	  if (commaIndex != -1) {
	    position.ret.push(position.data.substring(0,commaIndex));
	    position.data = trimHead(position.data.substring(commaIndex + 1));
	  }
	  else {
	    position.ret.push(position.data);
	    done = true;
	  }
	}
      }
      for (var i = 0; i < position.ret.length; i++) {
	position.ret[i] = unescapeQuote(position.ret[i]);
      }
      return position.ret;
    },
    
    /**
     * Remove leading and trailing spaces from the given string
     *
     * @param str {String} 
     */
    trim : function (str) {
      return str.replace(/^\s+|\s+$/g,'');
    },
    /**
     * Parse the given data into an array of lines
     * @param regExp {RegExp} A regular expression object that looks for new lines
     * @param data {String} String containing the CSV data.
     * 
     */
    lines : function (regExp, data) {
      var lines = data.split(regExp);
      var i = 0;
      while (i < lines.length) {
	// Remove leading newline rows
	if ('' == lines[i].replace(regExp, '')) {
	  lines.splice(i,1);
	  continue;
	}
	// If the leading line has an odd number of quotes,
	// it must be continued onto the next line.
	var quotes = lines[i].match(/\"/g);
	if (quotes && 1 == quotes.length % 2) {
	  if (i + 2 >= lines.length) {
	    alert('CSV content is probably invalid: odd number of quotes');
	    return lines;
	  }
	  lines[i] += lines[i+1] + lines[i+2];
	  lines.splice(i+1,2);
	} else {
	  i++;
	}
      }
      return lines;
    },
    /**
     * The default way of determining where there are more subtasks
     */
    defaultContinueCase : function (userData) {
	return userData.context.keepGoing;
    },
    /**
     * A subtask that breaks up the given string in lines and parses the fields one line at a time. 
     *
     * @param runRegExp {Function} A function that runs a regular expression object on the given data
     * @param returnArray {Array} The array that will contain the parsed lines                          
     * @param data {String} The CSV data to be parsed.
     */
    linesProgressive : function (runRegExp, returnArray, data) {
      return function (context) {
	context.regexp = runRegExp(data);
	if (context.regexp != null) {
	  var fields = csvparser.CSVParser.fields(context.regexp.input.substring(context.curr,context.regexp.index));
	  returnArray.push(fields);
	  
	  context.curr = context.regexp.index + context.regexp[0].length;
	}
	else {
	  context.keepGoing = false;
	  if (context.curr != data.length) {
	    var fields = csvparser.CSVParser.fields(data.substring(context.curr));
	    returnArray.push(fields);
	  }
	}
      };
    },
    /**
     * Parse out the fields in each of the given lines.
     * @param lines {[Array]} A list of lines in the CSV dataset
     */
    linesToRows : function (lines) {
      for (var i = 0; i < lines.length; i++){
	lines[i] = csvparser.CSVParser.fields(lines[i]);
      }
      return lines;
    }
  },
    
  members : {
    /**
     * Parse the given CSV dataset in one record at a time, giving control back to the browser
     * after each one has been parsed.
     * @param returnArray {[Array]} The array that will contain the result.
     * @param data {String} The CSV dataset
     */
    csvToArrayProgressive : function (returnArray, data) {
      var that = this;
      var regExp = function(data) {
	return that.getEolRegExp().exec(data);
      };
      var context = {
	curr : 0,
	regexp : null,
	keepGoing : true
      };
      return {
	context : context,
	func : csvparser.CSVParser.linesProgressive(regExp,returnArray,data),
	continueCase : csvparser.CSVParser.defaultContinueCase
      };
    },

    /**
     * Parse the given CSV dataset.
     * @param data {String} The CSV dataset
     */
    csvToArray : function (data) {
      return csvparser.CSVParser.linesToRows(csvparser.CSVParser.lines(this.getEolRegExp(),data));
    },

    tests : function () {
      var testLines = "test\ntest2\r\ntest3";
      var testFields = "\"test\",1234,,\"test,\"\"\"";
      var testCsv = "test\",1234\n,\r\n,\"test,\"\"\"";
      this.debug(this.lines(testLines,""));
      this.debug(csvparser.CSVParser.fields(testFields));
      this.debug(csvparser.CSVParser.csvToArray(testCsv));
    }
  }
});
